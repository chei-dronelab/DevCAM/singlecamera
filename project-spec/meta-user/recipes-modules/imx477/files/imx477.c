// SPDX-License-Identifier: GPL-2.0
/*
 * imx477.c - imx477 CMOS Image Sensor driver
 *
 * Copyright (C) 2017, Leopard Imaging, Inc.
 *
 * Leon Luo <leonl@leopardimaging.com>
 * Edwin Zou <edwinz@leopardimaging.com>
 * Luca Ceresoli <luca@lucaceresoli.net>
 */

#include <linux/clk.h>
#include <linux/delay.h>
#include <linux/gpio.h>
#include <linux/gpio/consumer.h>
#include <linux/i2c.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/of_gpio.h>
#include <linux/regmap.h>
#include <linux/slab.h>
#include <linux/v4l2-mediabus.h>
#include <linux/videodev2.h>

#include <media/v4l2-ctrls.h>
#include <media/v4l2-device.h>
#include <media/v4l2-subdev.h>
#include "imx477.h"
#include "imx477_mode_tbls.h"

/*
 * See "SHR, SVR Setting" in datasheet
 */
#define IMX477_DEFAULT_FRAME_LENGTH		(1551)
#define IMX477_MAX_FRAME_LENGTH			(0xB292)


#define IMX477_GAIN_SHIFT			(8)

/*
 * See "Analog Gain" and "Digital Gain" in datasheet
 * min gain is 1X
 * max gain is calculated based on imx477_GAIN_REG_MAX
 */
#define IMX477_GAIN_REG_MAX			(978)
#define IMX477_MIN_GAIN				(1)
#define IMX477_MAX_ANALOG_GAIN		(22)
#define IMX477_MAX_DIGITAL_GAIN		(16)
#define IMX477_DEF_GAIN				IMX477_MIN_GAIN
#define IMX477_GAIN_CONST			(1024) /* for gain formula */


#define IMX477_MAX_WIDTH			(4000)
#define IMX477_MAX_HEIGHT			(3040)
#define IMX477_MAX_FRAME_RATE			(60)
#define IMX477_MIN_FRAME_RATE			(5)
#define IMX477_DEF_FRAME_RATE			(15)

/*
 * register SHR is limited to (SVR value + 1) x VMAX value - 4
 */
#define IMX477_SHR_LIMIT_CONST			(4)
#define IMX477_SHR_ADDR_LSB				0x0203
#define IMX477_SHR_ADDR_MSB				0x0202
/*
 * Min and max sensor reset delay (microseconds)
 */
#define IMX477_RESET_DELAY1			(2000)
#define IMX477_RESET_DELAY2			(2200)

/*
 * shift and mask constants
 */
#define IMX477_SHIFT_8_BITS			(8)
#define IMX477_SHIFT_16_BITS			(16)
#define IMX477_MASK_LSB_2_BITS			(0x03)
#define IMX477_MASK_LSB_8_BITS			(0x00ff)

#define DRIVER_NAME "imx477"

#define IMX477_ANALOG_GAIN_ADDR_LSB		0x0205 /* ANALOG GAIN LSB */
#define IMX477_ANALOG_GAIN_ADDR_MSB		0x0204 /* ANALOG GAIN MSB */
#define IMX477_DIGITAL_GAIN_REG_LOW		0x020F /* Digital Gain */
#define IMX477_DIGITAL_GAIN_REG_HIG		0x020E /* Digital Gain */


static const struct regmap_config imx477_regmap_config = {
	.reg_bits = 16,
	.val_bits = 8,
	.cache_type = REGCACHE_RBTREE,
};
static inline void imx477_get_frame_length_regs(imx477_reg *regs,
				u32 frame_length)
{
	regs->addr = IMX477_VMAX_ADDR_MSB;
	regs->val = (frame_length >> 8) & 0xff;

	(regs + 1)->addr = IMX477_VMAX_ADDR_LSB;
	(regs + 1)->val = (frame_length) & 0xff;
}

static inline void imx477_get_coarse_time_regs(imx477_reg *regs,
				u32 coarse_time)
{
	regs->addr = IMX477_SHR_ADDR_MSB;
	regs->val = (coarse_time >> 8) & 0xff;

	(regs + 1)->addr = IMX477_SHR_ADDR_LSB;
	(regs + 1)->val = coarse_time & 0xff;
}


static inline void imx477_calculate_gain_regs(imx477_reg *regs,
				u16 gain)
{
	regs->addr = IMX477_ANALOG_GAIN_ADDR_MSB;
	regs->val = (gain >> 8) & IMX477_MASK_LSB_2_BITS;
	(regs + 1)->addr = IMX477_ANALOG_GAIN_ADDR_LSB;
	(regs + 1)->val = gain & 0xff;
}

static int test_mode;
module_param(test_mode, int, 0644);


/*
 * struct imx477_ctrls - imx477 ctrl structure
 * @handler: V4L2 ctrl handler structure
 * @exposure: Pointer to expsure ctrl structure
 * @gain: Pointer to gain ctrl structure
 * @vflip: Pointer to vflip ctrl structure
 * @test_pattern: Pointer to test pattern ctrl structure
 */
struct imx477_ctrls {
	struct v4l2_ctrl_handler handler;
	struct v4l2_ctrl *s_stream;
	struct v4l2_ctrl *gain;

};

/*
 * struct stim274 - imx477 device structure
 * @sd: V4L2 subdevice structure
 * @pad: Media pad structure
 * @client: Pointer to I2C client
 * @ctrls: imx477 control structure
 * @crop: rect to be captured
 * @compose: compose rect, i.e. output resolution
 * @format: V4L2 media bus frame format structure
 *          (width and height are in sync with the compose rect)
 * @frame_rate: V4L2 frame rate structure
 * @regmap: Pointer to regmap structure
 * @reset_gpio: Pointer to reset gpio
 * @lock: Mutex structure
 * @mode: Parameters for the selected readout mode
 */
struct stimx477 {
	struct v4l2_subdev sd;
	struct media_pad pad;
	struct i2c_client *client;
	struct imx477_ctrls ctrls;
	struct v4l2_mbus_framefmt format;
	struct v4l2_fract frame_interval;
	struct regmap *regmap;
	struct mutex lock; /* mutex lock for operations */
	u32 frame_length;;
};

#define imx477_ROUND(dim, step, flags)			\
	((flags) & V4L2_SEL_FLAG_GE			\
	 ? roundup((dim), (step))			\
	 : ((flags) & V4L2_SEL_FLAG_LE			\
	    ? rounddown((dim), (step))			\
	    : rounddown((dim) + (step) / 2, (step))))

/*
 * Function declaration
 */
static int imx477_set_gain(struct stimx477 *priv, struct v4l2_ctrl *ctrl);
static inline void msleep_range(unsigned int delay_base)
{
	//usleep_range(delay_base * 1000, delay_base * 1000 + 500);
	msleep(delay_base);
}

/*
 * v4l2_ctrl and v4l2_subdev related operations
 */

static inline struct v4l2_subdev *ctrl_to_sd(struct v4l2_ctrl *ctrl)
{
	return &container_of(ctrl->handler,
			     struct stimx477, ctrls.handler)->sd;
}

static inline struct stimx477 *to_imx477(struct v4l2_subdev *sd)
{
	return container_of(sd, struct stimx477, sd);
}

/*
 * Writing a register table
 *
 * @priv: Pointer to device
 * @table: Table containing register values (with optional delays)
 *
 * This is used to write register table into sensor's reg map.
 *
 * Return: 0 on success, errors otherwise
 */
static int imx477_write_table(struct stimx477 *priv, const struct reg_8 table[])
{
	struct regmap *regmap = priv->regmap;
	int err = 0;
	const struct reg_8 *next;
	u8 val;

	int range_start = -1;
	int range_count = 0;
	u8 range_vals[16];
	int max_range_vals = ARRAY_SIZE(range_vals);

	for (next = table;; next++) {
		//dev_err(&priv->client->dev,
		//	"%s : Table iteration", __func__);
		if ((next->addr != range_start + range_count) ||
		    (next->addr == IMX477_TABLE_END) ||
		    (next->addr == IMX477_TABLE_WAIT_MS) ||
		    (range_count == max_range_vals)) {
			if (range_count == 1)
				err = regmap_write(regmap,
						   range_start, range_vals[0]);
			else if (range_count > 1)
				err = regmap_bulk_write(regmap, range_start,
							&range_vals[0],
							range_count);

			if (err)
				return err;

			range_start = -1;
			range_count = 0;

			/* Handle special address values */
			if (next->addr == IMX477_TABLE_END)
				break;

			if (next->addr == IMX477_TABLE_WAIT_MS) {
				msleep_range(next->val);
				continue;
			}
		}

		val = next->val;

		if (range_start == -1)
			range_start = next->addr;

		range_vals[range_count++] = val;
	}
	return 0;
}

static inline int imx477_write_reg(struct stimx477 *priv, u16 addr, u8 val)
{
	int err;

	err = regmap_write(priv->regmap, addr, val);
	if (err)
		dev_err(&priv->client->dev,
			"%s : i2c write failed, %x = %x\n", __func__,
			addr, val);
	else
		dev_dbg(&priv->client->dev,
			"%s : addr 0x%x, val=0x%x\n", __func__,
			addr, val);
	return err;
}

static inline int imx477_read_reg(struct stimx477 *priv,
				u16 addr, u8 *val)
{
	int err = 0;
	u32 reg_val = 0;

	err = regmap_read(priv->regmap, addr, &reg_val);
	*val = reg_val & 0xFF;

	return err;
}
/**
 * Read a multibyte register.
 *
 * Uses a bulk read where possible.
 *
 * @priv: Pointer to device structure
 * @addr: Address of the LSB register.  Other registers must be
 *        consecutive, least-to-most significant.
 * @val: Pointer to store the register value (cpu endianness)
 * @nbytes: Number of bytes to read (range: [1..3]).
 *          Other bytes are zet to 0.
 *
 * Return: 0 on success, errors otherwise
 */
static int imx477_read_mbreg(struct stimx477 *priv, u16 addr, u32 *val,
			     size_t nbytes)
{
	__le32 val_le = 0;
	int err;

	err = regmap_bulk_read(priv->regmap, addr, &val_le, nbytes);
	if (err) {
		dev_err(&priv->client->dev,
			"%s : i2c bulk read failed, %x (%zu bytes)\n",
			__func__, addr, nbytes);
	} else {
		*val = le32_to_cpu(val_le);
		dev_dbg(&priv->client->dev,
			"%s : addr 0x%x, val=0x%x (%zu bytes)\n",
			__func__, addr, *val, nbytes);
	}

	return err;
}

/**
 * Write a multibyte register.
 *
 * Uses a bulk write where possible.
 *
 * @priv: Pointer to device structure
 * @addr: Address of the LSB register.  Other registers must be
 *        consecutive, least-to-most significant.
 * @val: Value to be written to the register (cpu endianness)
 * @nbytes: Number of bytes to write (range: [1..3])
 */
static int imx477_write_mbreg(struct stimx477 *priv, u16 addr, u32 val,
			      size_t nbytes)
{
	__le32 val_le = cpu_to_le32(val);
	int err;

	err = regmap_bulk_write(priv->regmap, addr, &val_le, nbytes);
	if (err)
		dev_err(&priv->client->dev,
			"%s : i2c bulk write failed, %x = %x (%zu bytes)\n",
			__func__, addr, val, nbytes);
	else
		dev_dbg(&priv->client->dev,
			"%s : addr 0x%x, val=0x%x (%zu bytes)\n",
			__func__, addr, val, nbytes);
	return err;
}

static int imx477_set_group_hold(struct stimx477 *priv, bool val)
{

	int err;

    return 0;
	err = imx477_write_reg(priv,
			       IMX477_GROUP_HOLD_ADDR, val);
	if (err) {
		dev_dbg(&priv->client->dev,
			"%s: Group hold control error\n", __func__);
		return err;
	}

	return 0;
}

/*
 * Set mode registers to start stream.
 * @priv: Pointer to device structure
 *
 * Return: 0 on success, errors otherwise
 */
static int imx477_mode_regs(struct stimx477 *priv)
{
	int err = 0;

	dev_err(&priv->client->dev,
	"%s: Writing Global Setting Mode Reg\n", __func__);
	err = imx477_write_table(priv, mode_table[IMX477_GLOBAL_SETTING]);
	if (err)
		return err;
	
	dev_err(&priv->client->dev,
	"%s: Writing Image Quality Reg\n", __func__);
	err = imx477_write_table(priv, mode_table[IMX477_IMAGE_QUALITY]);
	if(err)
		return err;

	dev_err(&priv->client->dev,
	"%s: Writing Stop Stream Reg\n", __func__);
	err = imx477_write_table(priv, mode_table[IMX477_MODE_STOP_STREAM]);
	if(err)
		return err;


	dev_err(&priv->client->dev,
	"%s: Writing Resolution Mode Reg\n", __func__);
	err = imx477_write_table(priv, mode_table[IMX477_MODE_4056X3040]);
	if (err)
		return err;


	//
	
	return err;
}

/*
 * imx477_start_stream - Function for starting stream per mode index
 * @priv: Pointer to device structure
 *
 * Return: 0 on success, errors otherwise
 */
static int imx477_start_stream(struct stimx477 *priv)
{
	int err = 0;

	/*
	 * Refer to "Standby Cancel Sequence when using CSI-2" in
	 * imx477 datasheet, it should wait 10ms or more here.
	 * give it 1 extra ms for margin
	 */
	//mutex_lock(&priv->lock);
	//msleep_range(1);
	msleep(22);
	dev_err(&priv->client->dev,
	"%s: Writing Stream Start Regs\n", __func__);
	err = imx477_write_table(priv, mode_table[IMX477_MODE_START_STREAM]);
	if (err){
		//mutex_unlock(&priv->lock);
		return err;
	}
	//mutex_unlock(&priv->lock);
	//msleep_range(1);
	msleep(22);
	return 0;
}

static int imx477_stop_streaming(struct stimx477 *priv)
{
	int err;

	//mutex_lock(&priv->lock);	
	dev_err(&priv->client->dev,
	"%s: Writing Stream Stop Regs\n", __func__);
	err = imx477_write_table(priv, mode_table[IMX477_MODE_STOP_STREAM]);
	if (err) {
		//mutex_unlock(&priv->lock);
		return err;
	}
	//mutex_unlock(&priv->lock);
	return 0;
}

static int imx477_s_stream(struct v4l2_subdev *sd, int on)
{
	struct stimx477 *imx477 = to_imx477(sd);
	int ret = 0;

	dev_dbg(&imx477->client->dev, "%s : %s\n", __func__,
		on ? "Stream Start" : "Stream Stop");

	mutex_lock(&imx477->lock);

	if (on) {
		/* load mode registers */
		dev_err(&imx477->client->dev, "%s : Writing Mode Regs\n", __func__);
		ret = imx477_mode_regs(imx477);
		if (ret)
			goto fail;
		dev_err(&imx477->client->dev, "%s : Stream Start Write\n", __func__);
		/* start stream */
		msleep(100);
		ret = imx477_start_stream(imx477);
		if (ret)
			goto fail;
	} else {
		/* stop stream */
		dev_err(&imx477->client->dev, "%s : Stream Stop Write\n", __func__);
		ret = imx477_stop_streaming(imx477);
		if (ret)
			goto fail;
	}

	mutex_unlock(&imx477->lock);
	dev_err(&imx477->client->dev, "%s : Done\n", __func__);
	return 0;

fail:
	mutex_unlock(&imx477->lock);
	dev_err(&imx477->client->dev, "s_stream failed\n");
	return ret;
}


static int imx477_s_stream_ctrl(struct v4l2_subdev *sd, int on)
{
	struct stimx477 *imx477 = to_imx477(sd);
	int ret = 0;

	dev_dbg(&imx477->client->dev, "%s : %s\n", __func__,
		on ? "Stream Start" : "Stream Stop");


	if (on) {
		/* load mode registers */
		dev_dbg(&imx477->client->dev, "%s : Writing Mode Regs\n", __func__);
		ret = imx477_mode_regs(imx477);
		if (ret)
			goto fail;
		dev_dbg(&imx477->client->dev, "%s : Stream Start Write\n", __func__);
		/* start stream */
		ret = imx477_start_stream(imx477);
		if (ret)
			goto fail;
	} else {
		/* stop stream */
		dev_dbg(&imx477->client->dev, "%s : Stream Stop Write\n", __func__);
		ret = imx477_stop_streaming(imx477);
		if (ret)
			goto fail;
	}

	dev_dbg(&imx477->client->dev, "%s : Done\n", __func__);
	return 0;

fail:
	dev_err(&imx477->client->dev, "s_stream failed\n");
	return ret;
}

/**
 * imx477_s_ctrl - This is used to set the imx477 V4L2 controls
 * @ctrl: V4L2 control to be set
 *
 * This function is used to set the V4L2 controls for the imx477 sensor.
 *
 * Return: 0 on success, errors otherwise
 */
static int imx477_s_ctrl(struct v4l2_ctrl *ctrl)
{
	struct v4l2_subdev *sd = ctrl_to_sd(ctrl);
	struct stimx477 *imx477 = to_imx477(sd);
	int ret = -EINVAL;

	dev_dbg(&imx477->client->dev,
		"%s : s_ctrl: %s, value: %d\n", __func__,
		ctrl->name, ctrl->val);

	switch (ctrl->id) {
		case V4L2_CID_GAIN:
			dev_dbg(&imx477->client->dev,
				"%s : set V4L2_CID_GAIN\n", __func__);
			ret = imx477_set_gain(imx477, ctrl);
			break;

	 	case V4L2_CID_MPEG_CX2341X_VIDEO_SPATIAL_FILTER:
			dev_dbg(&imx477->client->dev,
				"%s : set Stream\n", __func__);
			ret = imx477_s_stream_ctrl(sd, ctrl->val);
			break;

		return ret;
	}
	return ret;
}

/**
 * imx477_get_fmt - Get the pad format
 * @sd: Pointer to V4L2 Sub device structure
 * @cfg: Pointer to sub device pad information structure
 * @fmt: Pointer to pad level media bus format
 *
 * This function is used to get the pad format information.
 *
 * Return: 0 on success
 */
static int imx477_get_fmt(struct v4l2_subdev *sd,
			  struct v4l2_subdev_pad_config *cfg,
			  struct v4l2_subdev_format *fmt)
{
	struct stimx477 *imx477 = to_imx477(sd);

	mutex_lock(&imx477->lock);
	fmt->format = imx477->format;
	mutex_unlock(&imx477->lock);
	return 0;
}



/**
 * imx477_load_default - load default control values
 * @priv: Pointer to device structure
 *
 * Return: 0 on success, errors otherwise
 */

static int imx477_load_default(struct stimx477 *priv)
{
	int ret;

	/* load default control values */
	priv->frame_interval.numerator = 1;
	priv->frame_interval.denominator = IMX477_DEF_FRAME_RATE;
	priv->ctrls.gain->val = IMX477_DEF_GAIN;

	/* update gain */
	ret = v4l2_ctrl_s_ctrl(priv->ctrls.gain, priv->ctrls.gain->val);
	if (ret)
		return ret;

	return 0;
}

/**
 * imx477_s_stream - It is used to start/stop the streaming.
 * @sd: V4L2 Sub device
 * @on: Flag (True / False)
 *
 * This function controls the start or stop of streaming for the
 * imx477 sensor.
 *
 * Return: 0 on success, errors otherwise
 */

/*
 * imx477_set_digital gain - Function called when setting digital gain
 * @priv: Pointer to device structure
 * @dgain: Value of digital gain.
 *
 * Digital gain has only 4 steps: 1x, 2x, 4x, and 8x
 *
 * Return: 0 on success
 */
static int imx477_set_digital_gain(struct stimx477 *priv, u32 dgain)
{
	int ret;

	ret = imx477_write_reg(priv, IMX477_DIGITAL_GAIN_REG_LOW,
			       dgain & IMX477_MASK_LSB_8_BITS);
	ret |= imx477_write_reg(priv, IMX477_DIGITAL_GAIN_REG_HIG,
			       (dgain >> 8) & IMX477_MASK_LSB_8_BITS);
	return ret;
}


/*
 * imx477_set_gain - Function called when setting gain
 * @priv: Pointer to device structure
 * @val: Value of gain. the real value = val << imx477_GAIN_SHIFT;
 * @ctrl: v4l2 control pointer
 *
 * Set the gain based on input value.
 * The caller should hold the mutex lock imx477->lock if necessary
 *
 * Return: 0 on success
 */
static int imx477_set_gain(struct stimx477 *priv, struct v4l2_ctrl *ctrl)
{
	int err, i;
	imx477_reg reg_list[2];
	u32 gain, analog_gain, digital_gain, gain_reg;

	gain = (u32)(ctrl->val * 256 / 1000000);
	if (gain > IMX477_MAX_DIGITAL_GAIN * IMX477_MAX_ANALOG_GAIN * 256)
		gain = IMX477_MAX_DIGITAL_GAIN * IMX477_MAX_ANALOG_GAIN * 256;
	else if (gain < IMX477_MIN_GAIN * 256)
		gain = IMX477_MIN_GAIN * 256;

	if(gain > (IMX477_MAX_ANALOG_GAIN << IMX477_GAIN_SHIFT)) {
		analog_gain = IMX477_MAX_ANALOG_GAIN << IMX477_GAIN_SHIFT;
		digital_gain = gain / IMX477_MAX_ANALOG_GAIN;
	} else {
		analog_gain = gain;
		digital_gain = 256;
	}
	
	dev_dbg(&priv->client->dev, "%s :val = %lld, digital gain = %d, analog gain = %d\n",
		 __func__, ctrl->val,digital_gain, analog_gain);

	err = imx477_set_digital_gain(priv, digital_gain);
	if (err)
		goto fail;

	/* convert to register value, refer to imx477 datasheet */
	gain_reg = (u32)IMX477_GAIN_CONST -
		(IMX477_GAIN_CONST << IMX477_GAIN_SHIFT) / analog_gain;
	if (gain_reg > IMX477_GAIN_REG_MAX)
		gain_reg = IMX477_GAIN_REG_MAX;

	imx477_calculate_gain_regs(reg_list, (u16)gain_reg);

	for (i = 0; i < ARRAY_SIZE(reg_list); i++) {
		err = imx477_write_reg(priv, reg_list[i].addr,
			 reg_list[i].val);
		if (err)
			goto fail;
	}
	return 0;

fail:
	dev_info(&priv->client->dev, "%s: GAIN control error\n", __func__);
	return err;
}
/*
static int imx477_set_frame_rate(struct stimx477 *priv, s64 val)
{
	imx477_reg reg_list[2];
	int err;
	u32 frame_length;
	const struct sensor_mode_properties *mode =
		&s_data->sensor_props.sensor_modes[s_data->mode_prop_idx];
	int i = 0;

	frame_length = mode->signal_properties.pixel_clock.val *
		mode->control_properties.framerate_factor /
		mode->image_properties.line_length / val;

	
	priv->frame_length = frame_length;
	if (priv->frame_length > IMX477_MAX_FRAME_LENGTH)
		priv->frame_length = IMX477_MAX_FRAME_LENGTH;
    
	dev_dbg(dev, "%s: val: %lld, frame_length: %d\n", __func__,
		val, priv->frame_length);

	imx477_get_frame_length_regs(reg_list, priv->frame_length);

	for (i = 0; i < 2; i++) {
		err = imx477_write_reg(priv->s_data, reg_list[i].addr,
			 reg_list[i].val);
		if (err)
			goto fail;
	}

	return 0;

fail:
	dev_dbg(dev, "%s: FRAME_LENGTH control error\n", __func__);
	return err;	
}

*/
/*
 * imx477_set_exposure - Function called when setting exposure time
 * @priv: Pointer to device structure
 * @val: Variable for exposure time, in the unit of micro-second
 *
 * Set exposure time based on input value.
 * The caller should hold the mutex lock imx477->lock if necessary
 *
 * Return: 0 on success
 */


static const struct v4l2_subdev_pad_ops imx477_pad_ops = {
	.get_fmt = imx477_get_fmt,
};

static const struct v4l2_subdev_video_ops imx477_video_ops = {
	.s_stream = imx477_s_stream,
};

static const struct v4l2_subdev_ops imx477_subdev_ops = {
	.pad = &imx477_pad_ops,
	.video = &imx477_video_ops,
};

static const struct v4l2_ctrl_ops imx477_ctrl_ops = {
	.s_ctrl	= imx477_s_ctrl,
	
};

static const struct of_device_id imx477_of_id_table[] = {
	{ .compatible = "sony,imx477" },
	{ }
};

static const struct v4l2_ctrl_config ctrl_stream = {
	.ops = &imx477_ctrl_ops,
	.id = V4L2_CID_MPEG_CX2341X_VIDEO_SPATIAL_FILTER,
	.name = "Stream",
	.type = V4L2_CTRL_TYPE_INTEGER,
	.min = 0,
	.max = 1,
	.step = 1,
};

MODULE_DEVICE_TABLE(of, imx477_of_id_table);

static const struct i2c_device_id imx477_id[] = {
	{ "imx477", 0 },
	{ }
};
MODULE_DEVICE_TABLE(i2c, imx477_id);

static int imx477_probe(struct i2c_client *client)
{
	struct v4l2_subdev *sd;
	struct stimx477 *imx477;
	int ret;

	/* initialize imx477 */
	imx477 = devm_kzalloc(&client->dev, sizeof(*imx477), GFP_KERNEL);
	if (!imx477)
		return -ENOMEM;

	mutex_init(&imx477->lock);

	/* initialize format */
	imx477->format.width = IMX477_MAX_WIDTH;
	imx477->format.height = IMX477_MAX_HEIGHT;
	imx477->format.field = V4L2_FIELD_NONE;
	imx477->format.code = MEDIA_BUS_FMT_SRGGB12_1X12;
	imx477->format.colorspace = V4L2_COLORSPACE_SRGB;
	imx477->frame_interval.numerator = 1;
	imx477->frame_interval.denominator = IMX477_DEF_FRAME_RATE;

	/* initialize regmap */
	imx477->regmap = devm_regmap_init_i2c(client, &imx477_regmap_config);
	if (IS_ERR(imx477->regmap)) {
		dev_err(&client->dev,
			"regmap init failed: %ld\n", PTR_ERR(imx477->regmap));
		ret = -ENODEV;
		goto err_regmap;
	}

	/* initialize subdevice */
	imx477->client = client;
	sd = &imx477->sd;
	v4l2_i2c_subdev_init(sd, client, &imx477_subdev_ops);
	sd->flags |= V4L2_SUBDEV_FL_HAS_DEVNODE | V4L2_SUBDEV_FL_HAS_EVENTS;

	/* initialize subdev media pad */
	imx477->pad.flags = MEDIA_PAD_FL_SOURCE;
	sd->entity.function = MEDIA_ENT_F_CAM_SENSOR;
	ret = media_entity_pads_init(&sd->entity, 1, &imx477->pad);
	if (ret < 0) {
		dev_err(&client->dev,
			"%s : media entity init Failed %d\n", __func__, ret);
		goto err_regmap;
	}

	/* initialize controls */
	ret = v4l2_ctrl_handler_init(&imx477->ctrls.handler, 4);
	if (ret < 0) {
		dev_err(&client->dev,
			"%s : ctrl handler init Failed\n", __func__);
		goto err_me;
	}

	imx477->ctrls.handler.lock = &imx477->lock;

	/* add new controls */
	imx477->ctrls.gain = v4l2_ctrl_new_std(
		&imx477->ctrls.handler,
		&imx477_ctrl_ops,
		V4L2_CID_GAIN, IMX477_MIN_GAIN,
		IMX477_MAX_DIGITAL_GAIN * IMX477_MAX_ANALOG_GAIN, 1,
		IMX477_DEF_GAIN);

	imx477->ctrls.s_stream = v4l2_ctrl_new_custom(
		&imx477->ctrls.handler,
		&ctrl_stream,
		imx477);

	imx477->sd.ctrl_handler = &imx477->ctrls.handler;
	if (imx477->ctrls.handler.error) {
		ret = imx477->ctrls.handler.error;
		goto err_ctrls;
	}

	/* setup default controls */
	ret = v4l2_ctrl_handler_setup(&imx477->ctrls.handler);
	if (ret) {
		dev_err(&client->dev,
			"Error %d setup default controls\n", ret);
		goto err_ctrls;
	}

	/* register subdevice */
	ret = v4l2_async_register_subdev(sd);
	if (ret < 0) {
		dev_err(&client->dev,
			"%s : v4l2_async_register_subdev faild %d\n",
			__func__, ret);
		goto err_ctrls;
	}
	dev_info(&client->dev, "imx477 : imx477 probe success !\n");
	return 0;

err_ctrls:
	v4l2_ctrl_handler_free(&imx477->ctrls.handler);
err_me:
	media_entity_cleanup(&sd->entity);
err_regmap:
	mutex_destroy(&imx477->lock);
	return ret;
}

static int imx477_remove(struct i2c_client *client)
{
	struct v4l2_subdev *sd = i2c_get_clientdata(client);
	struct stimx477 *imx477 = to_imx477(sd);

	/* stop stream */
	imx477_write_table(imx477, imx477_stop);

	v4l2_async_unregister_subdev(sd);
	v4l2_ctrl_handler_free(&imx477->ctrls.handler);
	media_entity_cleanup(&sd->entity);
	mutex_destroy(&imx477->lock);
	return 0;
}

static struct i2c_driver imx477_i2c_driver = {
	.driver = {
		.name	= DRIVER_NAME,
		.of_match_table	= imx477_of_id_table,
	},
	.probe_new	= imx477_probe,
	.remove		= imx477_remove,
	.id_table	= imx477_id,
};

module_i2c_driver(imx477_i2c_driver);

MODULE_AUTHOR("Leon Luo <leonl@leopardimaging.com>");
MODULE_DESCRIPTION("imx477 CMOS Image Sensor driver");
MODULE_LICENSE("GPL v2");
