/**
 * Copyright (c) 2016-2019, NVIDIA Corporation.  All rights reserved.
 *
 * Copyright (c) 2016-2019, Guoxin Wu <guoxinw@leopardimaging.com>.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __IMX477_H__
#define __IMX477_H__

#define IMX477_SHR_ADDR_LSB				0x0203
#define IMX477_SHR_ADDR_MSB				0x0202

#define IMX477_VMAX_ADDR_LSB			0x0341
#define IMX477_VMAX_ADDR_MSB			0x0340

#define IMX477_GAIN_ADDR_LSB			0x0205
#define IMX477_GAIN_ADDR_MSB			0x0204

#define IMX477_GROUP_HOLD_ADDR			0x0104

#define IMX477_PIXEL_CLK_HZ				840000000
#define IMX477_LINE_LENGTH				6524

#define IMX477_VMAX						6524
#define IMX477_HMAX						263
#define IMX477_MODE1_OFFSET				112
#define IMX477_MODE1_SHR_MIN			12
#define IMX477_ET_FACTOR				400

struct camera_common_frmfmt {
	struct v4l2_frmsize_discrete	size;
	const int	*framerates;
	int	num_framerates;
	bool	hdr_en;
	int	mode;
};
struct imx477_power_rail {
	struct regulator *dvdd;
	struct regulator *avdd;
	struct regulator *iovdd;
	struct regulator *ext_reg1;
	struct regulator *ext_reg2;
	struct clk *mclk;
	unsigned int pwdn_gpio;
	unsigned int cam1_gpio;
	unsigned int reset_gpio;
	unsigned int af_gpio;
};

struct imx477_platform_data {
	const char *mclk_name; /* NULL for default default_mclk */
	unsigned int cam1_gpio;
	unsigned int reset_gpio;
	unsigned int af_gpio;
	bool ext_reg;
	int (*power_on)(struct imx477_power_rail *pw);
	int (*power_off)(struct imx477_power_rail *pw);
};

#endif  /* __IMX477_H__ */
